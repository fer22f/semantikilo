import { Context, Tree, Type, createVariable, getNextAvailableScope, getNextAvailableId, typeFromClass } from "../utils";

export type StartSuggestion = {
  op: "start"
  text: string
  arg: number
};

export function suggestStart(context: Context, tree: Tree, i: number) {
  const forest = context.forest.value;
  const suggestions = [];
  if (typeof tree.cat.cls === "object" && "op" in tree.cat.cls && tree.cat.cls.op === "scope") {
    suggestions.push({
      text: "Introduzir escopo",
      op: "start",
      arg: i
    } as StartSuggestion);
  }
  return suggestions;
}

export function applyStart(context: Context, { arg }: StartSuggestion) {
  const forest = context.forest.value;
  const { cls } = forest[arg].cat;
  if (!(typeof cls === "object" && "op" in cls && cls.op === "scope")) {
    throw new Error("Invalid apply start");
  }

  const scope = getNextAvailableScope(context);
  const type = typeFromClass(context, cls.res);
  forest[arg] = {
    expr: forest[arg].expr,
    cat: {
      cls: cls.res,
      interp: {
        value: createVariable(context, type),
        type,
      }
    },
    const: [forest[arg]],
    scope,
    id: getNextAvailableId(context),
    indicator: `I{\\Uparrow^{${scope}}}`
  }
}
