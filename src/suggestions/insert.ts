import { Context, Tree, getNextAvailableId } from "../utils";

function checkBeforeHypothesis(tree: Tree) {
  const hypothesis = {} as { [hyp: number]: Tree };

  if (tree.const) {
    const first = tree.const[0];
    if (first.hyp) {
      hypothesis[first.hyp] = first;
    } else {
      Object.entries(checkBeforeHypothesis(first)).forEach(([key, value]) => {
        hypothesis[+key] = value;
      });
    }
  }

  if (tree.ihyp) {
    delete hypothesis[tree.ihyp];
  }

  return hypothesis;
}

function checkAfterHypothesis(tree: Tree) {
  const hypothesis = {} as { [hyp: number]: Tree };

  if (tree.const) {
    const last = tree.const[tree.const.length-1];
    if (last.hyp) {
      hypothesis[last.hyp] = last;
    } else {
      Object.entries(checkAfterHypothesis(last)).forEach(([key, value]) => {
        hypothesis[+key] = value;
      });
    }
  }

  if (tree.ihyp) {
    delete hypothesis[tree.ihyp];
  }

  return hypothesis;
}

export type InsertSuggestion = {
  op: "insert";
  dir: "before" | "after";
  text: string
  arg: number;
  hyp: number;
};

export function suggestInsert(context: Context, tree: Tree, i: number) {
  const suggestions = [];

  const beforeHypothesis = Object.values(checkBeforeHypothesis(tree));
  for (const hypothesis of beforeHypothesis) {
    suggestions.push({
      text: `Introduzir $\\textcolor{red}{\\backslash}$ (hipótese ${hypothesis.hyp})`,
      op: "insert",
      dir: "before",
      arg: i,
      hyp: hypothesis.hyp
    } as InsertSuggestion);
  }

  const afterHypothesis = Object.values(checkAfterHypothesis(tree));
  for (const hypothesis of afterHypothesis) {
    suggestions.push({
      text: `Introduzir $\\textcolor{red}{/}$ (hipótese ${hypothesis.hyp})`,
      op: "insert",
      dir: "after",
      arg: i,
      hyp: hypothesis.hyp
    } as InsertSuggestion);
  }

  return suggestions;
}

export function applyInsert(context: Context, { arg, hyp: hypId, dir }: InsertSuggestion) {
  const forest = context.forest.value;
  const checkHypothesis = (dir === "before" ? checkBeforeHypothesis : checkAfterHypothesis);
  const hyp = checkHypothesis(forest[arg])[hypId];

  if (!("type" in hyp.cat.interp && "value" in hyp.cat.interp)) {
    throw new Error("Hypothesis not a value");
  }

  forest[arg] = {
    expr: forest[arg].expr,
    cat: {
      cls: {
        op: "apply",
        dir,
        res: forest[arg].cat.cls,
        arg: hyp.cat.cls,
      },
      interp: {
        op: "lambda",
        var: hyp.cat.interp,
        arg: forest[arg].cat.interp,
        type: [hyp.cat.interp.type, forest[arg].cat.interp.type]
      },
    },
    const: [forest[arg]],
    ihyp: hypId,
    id: getNextAvailableId(context),
    indicator: `I${dir === "after" ? "/" : "\\backslash"}^{${hypId}}`,
  }
}
