import { Ref } from "vue";
import { deepEqual } from "fast-equals";
import { Context, Category, Tree, betaReduction, getNextAvailableId } from "../utils";

function checkScopes(cat: Category, tree: Tree) {
  const scopes = {} as { [scope: number]: Tree };

  if (tree.scope && Array.isArray(tree.const)) {
    const { cls } = tree.const[0].cat;
    if (typeof cls === "object" && "op" in cls && cls.op === "scope" && deepEqual(cls.arg, cat.cls)) {
      scopes[tree.scope] = tree;
    }
  }

  if (tree.const) {
    for (const c of tree.const) {
      Object.entries(checkScopes(cat, c)).forEach(([key, value]) => {
        scopes[+key] = value;
      });
    }
  }

  if (tree.iscope) {
    delete scopes[tree.iscope];
  }

  return scopes;
}

export type EndSuggestion = {
  op: "end"
  text: string
  arg: number
  scope: number
}
export function suggestEnd(context: Context, tree: Tree, i: number) {
  const scopes = Object.values(checkScopes(tree.cat, tree));
  const suggestions = [];
  for (const scope of scopes) {
    suggestions.push({
      text: `Eliminar escopo ${scope.scope}`,
      op: "end",
      arg: i,
      scope: scope.scope
    } as EndSuggestion);
  }
  return suggestions;
}

export function applyEnd(context: Context, { arg, scope: scopeId }: EndSuggestion) {
  const forest = context.forest.value;
  const scope = checkScopes(forest[arg].cat, forest[arg])[scopeId];

  if (!(scope && "const" in scope && Array.isArray(scope.const) && scope.const.length === 1)) {
    throw new Error("Not a scope");
  }
  const nested = scope.const[0];

  if (!Array.isArray(nested.cat.interp.type)) {
    throw new Error("Invalid type");
  }

  if (!("value" in scope.cat.interp && "type" in scope.cat.interp)) {
    throw new Error("Scope not a value");
  }

  forest[arg] = {
    expr: forest[arg].expr,
    cat: {
      cls: forest[arg].cat.cls,
      interp: betaReduction({
        op: "apply",
        res: nested.cat.interp,
        arg: {
          op: "lambda",
          var: scope.cat.interp,
          arg: forest[arg].cat.interp,
          type: [scope.cat.interp.type, forest[arg].cat.interp.type]
        },
        type: nested.cat.interp.type[1]
      })
    },
    const: [forest[arg]],
    iscope: scopeId,
    id: getNextAvailableId(context),
    indicator: `E{\\Uparrow^{${scopeId}}}`
  }
}
