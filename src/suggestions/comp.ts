import { Ref } from "vue";
import { deepEqual } from "fast-equals";
import { Tree, Context, betaReduction, getNextAvailableId } from "../utils";

function checkAnyHypothesis(tree: Tree) {
  const hypothesis = {} as { [hyp: number]: Tree };

  if (tree.const) {
    for (const c of tree.const) {
      if (c.hyp) {
        hypothesis[c.hyp] = c;
      }
      Object.entries(checkAnyHypothesis(c)).forEach(([key, value]) => {
        hypothesis[+key] = value;
      });
    }
  }

  if (tree.ihyp) {
    delete hypothesis[tree.ihyp];
  }

  return hypothesis;
}

export type CompSuggestion = {
  op: "comp"
  text: string
  arg: number
  hyp: number
}
export function suggestComp(context: Context, tree: Tree, i: number) {
  const anyHypothesis = Object.values(checkAnyHypothesis(tree));
  const suggestions = [];
  for (const hypothesis of anyHypothesis) {
    suggestions.push({
      text: `Introduzir $\\textcolor{red}{\\uparrow}$ (hipótese ${hypothesis.hyp})`,
      op: "comp",
      arg: i,
      hyp: hypothesis.hyp
    } as CompSuggestion);
  }
  return suggestions;
}

export function applyComp(context: Context, { arg, hyp: hypId }: CompSuggestion) {
  const forest = context.forest.value;
  const hyp = checkAnyHypothesis(forest[arg])[hypId];
  if (!hyp) {
    throw new Error("Unknown hypothesis");
  }
  if (!("type" in hyp.cat.interp && "value" in hyp.cat.interp)) {
    throw new Error("Hypothesis not a value");
  }

  const argCat = forest[arg].cat;
  forest[arg] = {
    expr: forest[arg].expr,
    cat: {
      cls: {
        op: "comp",
        res: argCat.cls,
        arg: hyp.cat.cls
      },
      interp: {
        op: "lambda",
        var: hyp.cat.interp,
        arg: argCat.interp,
        type: [hyp.cat.interp.type, argCat.interp.type]
      }
    },
    const: [forest[arg]],
    ihyp: hypId,
    id: getNextAvailableId(context),
    indicator: `I{\\uparrow^{${hypId}}}`
  }
}
