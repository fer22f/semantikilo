import { Ref } from "vue";
import { deepEqual } from "fast-equals";
import { Tree, Context, Class, betaReduction, createVariable, typeFromClass, getNextAvailableHypothesis, getNextAvailableId } from "../utils";

function checkIds(cls: Class, tree: Tree): { ids: { [id: number]: Tree }, hyps: { [id: number]: Tree } } {
  const ids = {} as { [id: number]: Tree };
  const hyps = {} as { [id: number]: Tree };

  if (tree.hyp) {
    hyps[tree.hyp] = tree;
  }

  if (tree.const) {
    for (const c of tree.const) {
      const res = checkIds(cls, c);
      Object.entries(res.ids).forEach(([key, value]) => { ids[+key] = value; });
      Object.entries(res.hyps).forEach(([key, value]) => { hyps[+key] = value; });
    }
  }

  if (tree.ihyp) {
    delete hyps[tree.ihyp];
  }

  if (Object.keys(hyps).length === 0 && deepEqual(tree.cat.cls, cls)) {
    ids[tree.id] = tree;
  }

  return { ids, hyps };
}

export type AnaphSuggestion = {
  op: "anaph"
  text: string
  arg: number
  id: number | "hyp"
}
export function suggestAnaph(context: Context, tree: Tree, i: number) {
  const suggestions = [];

  const { cls, interp } = tree.cat;
  if (typeof cls === "object") {
    if (cls.op === "anaph") {
      const ids = {} as { [id: number]: Tree };

      for (let j = 0; j < i; j++) {
        Object.entries(checkIds(cls.arg, context.forest.value[j]).ids).forEach(([key, value]) => {
          ids[+key] = value;
        });
      }

      for (const [id, matchingClass] of Object.entries(ids)) {
        if (!deepEqual(matchingClass.cat.interp.type, interp.type[0])) { continue }

        suggestions.push({
          text: `Eliminar anáfora (${id})`,
          op: "anaph",
          arg: i,
          id: id,
        })
      }

      suggestions.push({
        text: "Eliminar anáfora (criando hipótese)",
        op: "anaph",
        arg: i,
        id: "hyp",
      });
    }
  }

  return suggestions;
}

export function applyAnaph(context: Context, { arg, id }: AnaphSuggestion) {
  const forest = context.forest.value;

  const argCat = forest[arg].cat;
  if (!(typeof argCat.cls === "object" && "op" in argCat.cls && argCat.cls.op === "anaph")) {
    throw new Error("Invalid elimination");
  }

  if (id === "hyp") {
    const id = getNextAvailableId(context);
    const hyp = getNextAvailableHypothesis(context);
    const type = typeFromClass(context, argCat.cls.arg);
    const anaph = {
      expr: null,
      cat: {
        cls: argCat.cls.arg,
        interp: {
          value: createVariable(context, type),
          type
        }
      },
      id,
      hyp,
      indicator: "H"
    };
    forest[arg] = {
      expr: forest[arg].expr,
      cat: {
        cls: argCat.cls.res,
        interp: betaReduction({
          op: "apply",
          res: argCat.interp,
          arg: anaph.cat.interp,
          type: argCat.interp.type[1],
        })
      },
      const: [forest[arg]],
      aid: id,
      hyp,
      id: getNextAvailableId(context),
      indicator: `E{|^${id}}`
    }
    forest.splice(arg, 0, anaph);
  } else {
    const ids = {} as { [id: number]: Tree };
    for (let j = 0; j < arg; j++) {
      Object.entries(checkIds(argCat.cls.arg, context.forest.value[j]).ids).forEach(([key, value]) => {
        ids[+key] = value;
      });
    }
    const anaph = ids[id];
    if (!anaph) {
      throw new Error("Unknown anaphora");
    }
    if (!deepEqual(anaph.cat.interp.type, argCat.interp.type[0])) {
      throw new Error("Type is incorrect");
    }

    forest[arg] = {
      expr: forest[arg].expr,
      cat: {
        cls: argCat.cls.res,
        interp: betaReduction({
          op: "apply",
          res: argCat.interp,
          arg: anaph.cat.interp,
          type: argCat.interp.type[1],
        })
      },
      const: [forest[arg]],
      aid: id,
      id: getNextAvailableId(context),
      indicator: `E{|^{${id}}}`
    }
  }
}
