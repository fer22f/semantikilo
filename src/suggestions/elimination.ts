import { deepEqual } from "fast-equals";
import { Tree, Context, betaReduction, concatExpr, Type, getNextAvailableId, createVariable, getNextAvailableHypothesis } from "../utils";

export type EliminationSuggestion = {
  op: "elim"
  dir: "before" | "after"
  text: string
  res: number
  arg: number | "hyp"
};

export function suggestElimination(context: Context, tree: Tree, i: number) {
  const forest = context.forest.value;
  const suggestions = [];

  const { cls } = tree.cat;
  if (typeof cls === "object") {
    if (cls.op === "apply") {
      if (cls.dir === "after") {
        if (deepEqual(cls.arg, forest[i+1]?.cat.cls)) {
          suggestions.push({
            text: "Eliminar $\\textcolor{red}{/}$",
            op: "elim",
            dir: "after",
            res: i,
            arg: i+1,
          } as EliminationSuggestion);
        }
        suggestions.push({
          text: "Eliminar $\\textcolor{red}{/}$ (criando hipótese)",
          op: "elim",
          dir: "after",
          res: i,
          arg: "hyp"
        } as EliminationSuggestion);
      } else if (cls.dir === "before") {
        if (deepEqual(cls.arg, forest[i-1]?.cat.cls)) {
          suggestions.push({
            text: "Eliminar $\\textcolor{red}{\\backslash}$",
            op: "elim",
            dir: "before",
            res: i,
            arg: i-1
          } as EliminationSuggestion);
        }
        suggestions.push({
          text: "Eliminar $\\textcolor{red}{\\backslash}$ (criando hipótese)",
          op: "elim",
          dir: "before",
          res: i,
          arg: "hyp"
        } as EliminationSuggestion);
      }
    }
  }

  return suggestions;
}

export function applyElimination(context: Context, { res, arg, dir }: EliminationSuggestion) {
  const forest = context.forest.value;
  const resCat = forest[res].cat;
  if (!(typeof resCat.cls === "object" && "op" in resCat.cls && resCat.cls.op === "apply")) {
    throw new Error("Not an apply class");
  }

  if (arg === "hyp") {
    const type = resCat.interp.type[0];
    const arg = {
      expr: null,
      cat: {
        cls: resCat.cls.arg,
        interp: {
          value: createVariable(context, type),
          type
        }
      },
      hyp: getNextAvailableHypothesis(context),
      id: getNextAvailableId(context),
      indicator: "H"
    };
    const a = dir === "after" ? forest[res] : arg;
    const b = dir === "after" ? arg : forest[res];
    forest[res] = {
      expr: forest[res].expr,
      cat: {
        cls: resCat.cls.res,
        interp: betaReduction({
          op: "apply",
          res: resCat.interp,
          arg: arg.cat.interp,
          type: resCat.interp.type[1]
        })
      },
      const: [a, b],
      id: getNextAvailableId(context),
      indicator: `E${dir === "after" ? "/" : "\\backslash"}`,
    };
  } else {
    const argCat = forest[arg].cat;
    if (!deepEqual(resCat.cls.arg, argCat.cls)) {
      throw new Error("Incorrect class");
    }
  
    if (!deepEqual(resCat.interp.type[0], argCat.interp.type)) {
      throw new Error("Incorrect type");
    }
  
    const a = dir === "after" ? res : arg;
    const b = dir === "after" ? arg : res;
  
    forest[res] = {
      expr: concatExpr(forest[a].expr, forest[b].expr),
      cat: {
        cls: resCat.cls.res,
        interp: betaReduction({
          op: "apply",
          res: resCat.interp,
          arg: argCat.interp,
          type: resCat.interp.type[1]
        })
      },
      const: [forest[a], forest[b]],
      id: getNextAvailableId(context),
      indicator: `E${dir === "after" ? "/" : "\\backslash"}`,
    };
    forest.splice(arg, 1);
  }
}
