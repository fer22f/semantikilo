import { Ref, ref, computed } from "vue";
import { deepEqual } from "fast-equals";

export function createContext(lexicon: Ref<{
  lexes: Array<Tree>,
  types: { [klass: string]: Type }
}>): Context {
  const forest = ref([]);

  return {
    lexes: computed({
      set(value) { lexicon.value.lexes = value; },
      get() { return lexicon.value.lexes },
    }),
    types: lexicon.value.types,
    forest,
  }
}

export function insertLex(context: Context, lex: Tree, i: number | null = null): Context {
  const forest = context.forest.value;
  forest.splice(i !== null ? i : forest.length, 0, {
    ...lex,
    cat: { ...lex.cat, interp: substituteMeta(context, lex.cat.interp, {}) },
    ..."hyp" in lex ? { hyp: getNextAvailableHypothesis(context) } : {},
    id: getNextAvailableId(context),
  });
  return context;
}

export type Context = {
  lexes: Ref<Array<Tree>>,
  types: { [klass: string]: Type },
  forest: Ref<Array<Tree>>,
};

export type Class = string | {
  op: "apply"
  dir: "after" | "before"
  res: Class
  arg: Class
} | {
  op: "scope" | "comp" | "anaph" | "prod" | "any"
  res: Class
  arg: Class
};

export type Type = string | [Type, Type];

export type Value = {
  meta?: true
  value: string
  type: Type
}

export type Interpretation = {
  op: "lambda" | "iota" | "forall" | "exists",
  var: Value,
  arg: Interpretation
  type: Type
} | {
  op: "and" | "apply" | "then" | "or" | "iff"
  res: Interpretation
  arg: Interpretation
  type: Type
} | {
  op: "not"
  arg: Interpretation
  type: Type
} | Value;

export type Category = {
  cls: Class
  interp: Interpretation
};

export type Tree = {
  expr: string | null;
  cat: Category
  const?: Array<Tree>
  indicator: string,
  scope?: number
  iscope?: number
  id: number
  aid?: number
  hyp?: number
  ihyp?: number
}

export function substituteVariable(interp: Interpretation, variableFrom: Value, variableTo: Interpretation): Interpretation {
  if ("value" in interp && interp.value === variableFrom.value) {
    return variableTo;
  }
  if ("op" in interp) {
    return {
      ...interp,
      ..."var" in interp && { var: substituteVariable(interp.var, variableFrom, variableTo) },
      ..."res" in interp && { res: substituteVariable(interp.res, variableFrom, variableTo) },
      ..."arg" in interp && { arg: substituteVariable(interp.arg, variableFrom, variableTo) },
    } as any
  }
  return interp;
}

export function betaReduction(interp: Interpretation): Interpretation {
  if ("op" in interp && interp.op === "apply" && "op" in interp.res && interp.res.op === "lambda") {
    if (!deepEqual(interp.res.var.type, interp.arg.type)) {
      throw new Error("Wrong type for beta reduction");
    }
    return betaReduction(substituteVariable(
      interp.res.arg,
      interp.res.var,
      interp.arg
    ));
  }
  if ("op" in interp) {
    return {
      ...interp,
      ..."res" in interp && { res: betaReduction(interp.res) },
      ..."arg" in interp && { arg: betaReduction(interp.arg) },
    } as any
  }
  return interp;
}

export function checkUsedVariableAttrs(interp: Interpretation): {
  [variable: string]: {
    type: Type
  } | {
    meta: true,
    type: Type
  }
} {
  if ("value" in interp) {
    return {
      [interp.value]: {
        ...interp.meta && { meta: true },
        type: interp.type
      }
    };
  }
  if ("op" in interp) {
    if (interp.op === "and" || interp.op === "apply" || interp.op === "then" || interp.op === "iff" || interp.op === "or") {
      return {
        ...checkUsedVariableAttrs(interp.res),
        ...checkUsedVariableAttrs(interp.arg),
      }
    } else if (interp.op === "not") {
      return checkUsedVariableAttrs(interp.arg);
    } else if (interp.op === "lambda" || interp.op === "exists" || interp.op === "iota" || interp.op === "forall") {
      return {
        ...checkUsedVariableAttrs(interp.var),
        ...checkUsedVariableAttrs(interp.arg),
      }
    }
  }
  return {};
}

export function createVariable(context: Context, type: Type, used: Set<string> = new Set()) {
  const forest = context.forest.value;
  function checkUsedVariablesInInterpretation(interp: Interpretation): Set<string> {
    if ("value" in interp) {
      return new Set([interp.value]);
    }
    if ("op" in interp) {
      if (interp.op === "and" || interp.op === "apply" || interp.op === "then" || interp.op === "iff" || interp.op === "or") {
        return new Set([
          ...checkUsedVariablesInInterpretation(interp.res),
          ...checkUsedVariablesInInterpretation(interp.arg),
        ])
      } else if (interp.op === "not") {
        return checkUsedVariablesInInterpretation(interp.arg);
      } else if (interp.op === "lambda" || interp.op === "exists" || interp.op === "iota" || interp.op === "forall") {
        return new Set([
          ...checkUsedVariablesInInterpretation(interp.var),
          ...checkUsedVariablesInInterpretation(interp.arg),
        ])
      }
    }
    return new Set();
  }

  function checkUsedVariables(tree: Tree): Set<string> {
    let variables = checkUsedVariablesInInterpretation(tree.cat.interp);

    if (tree.const) {
      return tree.const.reduce((acc, tree) => new Set([...acc, ...checkUsedVariables(tree)]), variables);
    } else {
      return variables;
    }
  }

  const usedVariables = new Set([
    ...Object.values(forest).reduce((acc, tree) => new Set([...acc, ...checkUsedVariables(tree)]), new Set()),
    ...used
  ]);

  if (type === "e") {
    let eType = "x";
    while (usedVariables.has(eType)) {
      if (eType === "z") {
        eType = "x_{0}";
      } else if (eType.length === 1) {
        eType = String.fromCodePoint(eType.charCodeAt(0)+1);
      } else {
        eType = `x_{${parseInt(eType.split("_")[1].slice(1, -1))+1}}`
      }
    }
    return eType;
  } else {
    let otherType = "P";
    while (usedVariables.has(otherType)) {
      if (otherType === "Z") {
        otherType = "P_{0}";
      } else if (otherType.length === 1) {
        otherType = String.fromCodePoint(otherType.charCodeAt(0)+1);
      } else {
        otherType = `P_{${parseInt(otherType.split("_")[1].slice(1, -1))+1}}`
      }
    }
    return otherType;
  }
}

export function concatExpr(expr1: string | null, expr2: string | null) {
  if (expr1 === null) { return expr2; }
  if (expr2 === null) { return expr1; }
  return `${expr1} ${expr2}`;
}

export function substituteMeta(context: Context, interp: Interpretation, variables: { [value: string]: Value }): Interpretation {
  if ("meta" in interp) {
    if (!variables[interp.value]) {
      variables[interp.value] = {
        value: createVariable(context, interp.type, new Set(Object.values(variables).map(x => x.value))),
        type: interp.type,
      };
    }
    return variables[interp.value];
  }
  if ("op" in interp) {
    if (interp.op === "and" || interp.op === "apply" || interp.op === "then" || interp.op === "iff" || interp.op === "or") {
      return {
        ...interp,
        res: substituteMeta(context, interp.res, variables),
        arg: substituteMeta(context, interp.arg, variables),
      }
    } else if (interp.op === "not") {
      return {
        ...interp,
        arg: substituteMeta(context, interp.arg, variables),
      }
    } else if (interp.op === "lambda" || interp.op === "exists" || interp.op === "iota" || interp.op === "forall") {
      return {
        ...interp,
        var: substituteMeta(context, interp.var, variables) as Value,
        arg: substituteMeta(context, interp.arg, variables),
      }
    }
  }
  return interp;
}

export function checkTypes(interp: Interpretation): Array<{
  text: string,
  context: Interpretation
}> {
  if ("op" in interp) {
    const errors = [];

    if (interp.op === "and" || interp.op === "then" || interp.op === "or" || interp.op === "iff") {
      if (interp.res.type !== "t") {
        errors.push({ text: "Expected type t at res", context: interp });
      }
      if (interp.arg.type !== "t") {
        errors.push({ text: "Expected type t at arg", context: interp });
      }
      if (interp.type !== "t") {
        errors.push({ text: "Expected type t", context: interp });
      }
    }

    if (interp.op === "lambda") {
      if (!deepEqual(interp.type, [interp.var.type, interp.arg.type])) {
        errors.push({ text: "Wrong type lambda", context: interp });
      }
    }

    if (interp.op === "apply") {
      if (!deepEqual(interp.res.type, [interp.arg.type, interp.type])) {
        errors.push({ text: "Wrong type apply", context: interp });
      }
    }

    if (interp.op === "iota" || interp.op === "exists" || interp.op === "forall" || interp.op === "not") {
      if (interp.arg.type !== "t") {
        errors.push({ text: "Expected type t at arg", context: interp });
      }
      if (interp.type !== "t") {
        errors.push({ text: "Expected type t", context: interp });
      }
    }

    if (interp.op === "and" || interp.op === "apply" || interp.op === "then" || interp.op === "iff" || interp.op === "or") {
      return [
        ...errors,
        ...checkTypes(interp.res),
        ...checkTypes(interp.arg),
      ]
    } else if (interp.op === "not") {
      return [
        ...errors,
        ...checkTypes(interp.arg),
      ]
    } else if (interp.op === "lambda" || interp.op === "exists" || interp.op === "iota" || interp.op === "forall") {
      return [
        ...errors,
        ...checkTypes(interp.var),
        ...checkTypes(interp.arg),
      ]
    }
  }

  if (!("type" in interp)) {
    return [{ text: "Missing type", context: interp }];
  }

  return [];
}

export function getNextAvailableHypothesis(context: Context) {
  const forest = context.forest.value;
  function checkAnyAvailableHypothesis(tree: Tree): Set<number> {
    const hypothesis: Set<number> = tree.hyp ? new Set([tree.hyp]) : new Set();

    if (tree.const) {
      return Object.values(tree.const).reduce((acc, tree) =>
          new Set([...acc, ...checkAnyAvailableHypothesis(tree)]), hypothesis);
    }

    return hypothesis;
  }

  const hypothesis = Object.values(forest).reduce((acc, tree) =>
    new Set([...acc, ...checkAnyAvailableHypothesis(tree)]), new Set());

  let next = 1;
  while (hypothesis.has(next)) { next++; }
  return next;
}

export function getNextAvailableScope(context: Context) {
  const forest = context.forest.value;
  function checkAnyAvailableScopes(tree: Tree): Set<number> {
    const scopes: Set<number> = tree.scope ? new Set([tree.scope]) : new Set();

    if (tree.const) {
      return Object.values(tree.const).reduce((acc, tree) =>
          new Set([...acc, ...checkAnyAvailableScopes(tree)]), scopes);
    }

    return scopes;
  }

  const scopes = Object.values(forest).reduce((acc, tree) =>
    new Set([...acc, ...checkAnyAvailableScopes(tree)]), new Set());

  let next = 1;
  while (scopes.has(next)) { next++; }
  return next;
}

export function getNextAvailableId(context: Context) {
  const forest = context.forest.value;
  function checkAnyAvailableIds(tree: Tree): Set<number> {
    const ids: Set<number> = tree.id ? new Set([tree.id]) : new Set();

    if (tree.const) {
      return Object.values(tree.const).reduce((acc, tree) =>
          new Set([...acc, ...checkAnyAvailableIds(tree)]), ids);
    }

    return ids;
  }

  const ids = Object.values(forest).reduce((acc, tree) =>
    new Set([...acc, ...checkAnyAvailableIds(tree)]), new Set());

  let next = 1;
  while (ids.has(next)) { next++; }
  return next;
}

export function typeFromClass(context: Context, cls: Class): Type {
  if (typeof cls === "string") {
    return context.types[cls];
  }
  return [typeFromClass(context, cls.arg), typeFromClass(context, cls.res)];
}
