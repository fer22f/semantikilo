import { Class, Category, Type, Tree, Interpretation, Value } from "./utils";

export function isKlass(value: unknown): value is Class {
  return (
    (typeof value === "string") ||
    (
      typeof value === "object" &&
      value !== null &&
      "op" in value &&
      (
        ["scope", "comp", "anaph", "any", "prod"].includes((value as { op: string }).op) ||
        ((value as { op: string }).op === "apply" && "dir" in value)
      )
    )
  );
}

export function latexKlassRaw(cls: Class): string {
  if (typeof cls === "string") { return cls; }

  let { res, arg } = cls;
  if (typeof res !== "string") { res = `(${latexKlassRaw(res)})`; }
  if (typeof arg !== "string") { arg = `(${latexKlassRaw(arg)})`; }
  if (cls.op === "apply") {
    const { dir } = cls;
    if (dir === "after") {
      return `${res} / ${arg}`
    } else {
      return `${arg} \\backslash ${res}`
    }
  } else if (cls.op === "scope") {
    return `${res} \\Uparrow ${arg}`;
  } else if (cls.op === "comp") {
    return `${res} \\uparrow ${arg}`;
  } else if (cls.op === "any") {
    return `${res} \\bowtie ${arg}`;
  } else if (cls.op === "prod") {
    return `${res} \\bullet ${arg}`;
  } else {
    return `${res} | ${arg}`;
  }
}

export function latexKlass(value: Class): string {
  return `\\textcolor{red}{${latexKlassRaw(value)}}`;
}

function quantifier(symbol: string, { var: v, arg }: { var: Value, arg: Interpretation }) {
  return `${symbol} ${latexInterpRaw(v)}. ${latexInterpRaw(arg)}`;
}

function binaryOp(symbol: string, { res, arg }: { res: Interpretation, arg: Interpretation }) {
  return `(${latexInterpRaw(res)} ${symbol} ${latexInterpRaw(arg)})`;
}

export function latexInterpRaw(interp: Interpretation): string {
  if ("op" in interp) {
    if (interp.op === "apply") {
      const { res, arg } = interp;
      return `(${latexInterpRaw(res)} \\; ${latexInterpRaw(arg)})`;
    } else if (interp.op === "lambda") {
      return quantifier("\\lambda", interp);
    } else if (interp.op === "iota") {
      return quantifier("\\iota", interp);
    } else if (interp.op === "forall") {
      return quantifier("\\forall", interp);
    } else if (interp.op === "exists") {
      return quantifier("\\exists", interp);
    } else if (interp.op === "and") {
      return binaryOp("\\wedge", interp);
    } else if (interp.op === "then") {
      return binaryOp("\\rightarrow", interp);
    } else if (interp.op === "or") {
      return binaryOp("\\vee", interp);
    } else if (interp.op === "iff") {
      return binaryOp("\\iff", interp);
    } else {
      const { arg } = interp;
      return `\\neg ${latexInterpRaw(arg)}`
    }
  }
  return interp.value;
}

export function isInterp(value: unknown): value is Interpretation {
  return (typeof value === "object" && value !== null) && (
    (
      "op" in value &&
        typeof (value as { op: unknown }).op === "string" && ["apply", "lambda", "iota", "forall", "exists", "and", "then", "not"].includes((value as { op: string }).op) && !("dir" in value)
    ) || (
      "value" in value && "type" in value
    )
  );
}

export function latexInterp(value: Interpretation): string {
  return `\\textcolor{blue}{${latexInterpRaw(value)}}`;
}

export function latexType(value: Type) {
  return `\\textcolor{purple}{${latexArrays(value)}}`;
}

export function latexInterpType(value: Interpretation) {
  return `${latexInterp(value)} : ${latexType(value.type)}`;
}

export function latexExpr(expr: string | null) {
  if (expr === null) {
    return `\\empty`;
  }
  return `\\text{\\textcolor{brown}{${expr}}}`;
}

export function latexLex(lex: Tree, showTypes: boolean = false) {
  return `\\langle ${
    latexExpr(lex.expr)
  }, ${lex.cat.cls ? latexKlass(lex.cat.cls) : "\\emptyset"}, ${
    lex.cat.interp ? latexInterp(lex.cat.interp) : "\\emptyset"
  }${showTypes ? ` : ${latexType(lex.cat.interp.type)}` : ""}\\rangle`;
}

export function latexCat(cat: Category, showTypes: boolean = false) {
  return `${latexKlass(cat.cls)} : ${latexInterp(cat.interp)}${
    showTypes ? ` : ${latexType(cat.interp.type)}` : ""}`;
}

export function latexArrays(obj: Category | Class | Interpretation | Type): string {
  if (typeof obj !== "object") {
    return obj.toString();
  }
  if (Array.isArray(obj)) {
    return `\\left\\langle ${obj.map(it => latexArrays(it)).join(",")} \\right\\rangle`;
  }
  return `\\begin{bmatrix*}[l] ${
    Object.entries(obj).flatMap(([key, value]) => {
      if (key === "indicator") { return []; }
      if (key === "expr") {
        return [`${key} & ${latexExpr(value)}`];
      }
      if (key === "type") {
        return [`${key} & \\textcolor{purple}{${latexArrays(value)}}`];
      }
      if (key === "value") {
        return [`${key} & \\textcolor{blue}{${value}}`];
      }
      if (typeof value === "string" && ["arg", "res", "cls"].includes(key)) {
        return [`${key} & \\textcolor{red}{${value}}`];
      }
      if (key === "op" && value === "and") {
        return [`${key} & \\wedge`];
      }
      if (key === "op" && value === "then") {
        return [`${key} & \\rightarrow`];
      }
      return [`${key} & ${latexArrays(value)}`];
    }).join(" \\\\\n")
  } \\end{bmatrix*}${"indicator" in obj ? `^{${(obj as { indicator: string }).indicator}}` : ""}`;
}
