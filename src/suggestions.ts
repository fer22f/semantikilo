import { Context, Tree, Type, substituteVariable, betaReduction } from "./utils";
import { EliminationSuggestion, suggestElimination, applyElimination } from "./suggestions/elimination";
import { InsertSuggestion, suggestInsert, applyInsert } from "./suggestions/insert";
import { StartSuggestion, suggestStart, applyStart } from "./suggestions/start";
import { EndSuggestion, suggestEnd, applyEnd } from "./suggestions/end";
import { CompSuggestion, suggestComp, applyComp } from "./suggestions/comp";
import { AnaphSuggestion, suggestAnaph, applyAnaph } from "./suggestions/anaph";

export function suggest(context: Context, tree: Tree, i: number) {
  return [
    ...suggestElimination(context, tree, i),
    ...suggestInsert(context, tree, i),
    ...suggestStart(context, tree, i),
    ...suggestEnd(context, tree, i),
    ...suggestComp(context, tree, i),
    ...suggestAnaph(context, tree, i),
  ];
}

export type Suggestion = InsertSuggestion | EliminationSuggestion | StartSuggestion | EndSuggestion | CompSuggestion | AnaphSuggestion;

export function applySuggestion(context: Context, suggestion: Suggestion) {
  if (suggestion.op === "elim") {
    applyElimination(context, suggestion);
  } else if (suggestion.op === "insert") {
    applyInsert(context, suggestion);
  } else if (suggestion.op === "start") {
    applyStart(context, suggestion);
  } else if (suggestion.op === "comp") {
    applyComp(context, suggestion);
  } else if (suggestion.op === "end") {
    applyEnd(context, suggestion);
  } else {
    applyAnaph(context, suggestion);
  }
}
